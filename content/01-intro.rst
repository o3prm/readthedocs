Introduction
------------

Probabilistic Relational Models (PRMs) are a fully object-oriented extension of
Bayesian Networks (BNs). Both PRMs and BNs are one of the many Probabilistic
Graphical Models. In this introduction, we will offer a very short
introduction to both models. For further reading, we suggest the reader to
lookup the references in the Bibliography section.

Bayesian Networks
~~~~~~~~~~~~~~~~~

Bayesian Networks represent discrete probability distributions using a
directed graph and parameters called Conditional Probability Tables. The
nodes of the graph represent discrete random variables and the arcs represent
probabilistic dependencies between those variables (to be precise, the lack of
an arc between two nodes represents a conditional independence between
the corresponding random variables). To each node is assigned the
Conditional Probability Tables (CPTs) of the node given its parents in the
graph, and the joint probability distribution over all the nodes/variables
in the graph is equal to the product of all the CPTs.

.. image:: images/water_sprinkler.png

The Water Sprinkler is a classic example of a Bayesian Networks. Its purpose
is to infer whether the grass in a garden is wet because of the rain or
because the house's owner forgot to turn off the water sprinkler.

The probability distribution is defined over four Boolean random variables:
``Cloudy``, ``Sprinkler``, ``Rain`` and ``Wet Grass``. There are also four CPTs
representing the following conditional distributions: ``P(Cloudy)``,
``P(Sprinkler|Cloudy)``, ``P(Rain|Cloudy)`` and ``P(Wet Grass | Sprinkler,
Rain)``.

We won't go further in our introduction of Bayesian Network and if you never
heard of Bayesian Networks, you might need to first familiarize with them 
before using O3PRM. A good place to start is the pyAgrum tutorials that you
can find at http://agrum.gitlab.io.

Probabilistic Relational Models
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Bayesian Networks suffer from the same issue that had early programming languages:
the larger the network, the harder it is to create it and to maintain it.
Naturally, the BN community looked at the solutions provided by the language
programming community and found several paradigms to extend BNs: first
order logics, entity-relation models, object oriented paradigms, etc. There
is no current consensus on which BN extension is the best as each one
offers specific features lacking in the other paradigms. 

Probabilistic Relational Models are one of several Object Oriented extensions
of Bayesian Networks. They offer a good implementation of the Object Oriented
paradigm while not diverging too much from the Bayesian Networks
framework. Their precise definition is provided in subsequent sections of
this documentation. But, for the moment, just keep in mind the fact that they
are an object-oriented extension of BNs.

Implementation
~~~~~~~~~~~~~~

The aGrUM framework offers an implementation of the O3PRM language. You can
either directly use aGrUM, which is written in C++, its Python wrapper
pyAgrum or the prm_run application shipped with aGrUM's source code. All
resources and installation instructions can be found at
http://agrum.gitlab.io.

