=======================
O3PRM project structure
=======================

The O3PRM language is made of *compilation units* that are placed into
*modules*. It is possible to encode in a single file an entire project but
it is not recommended. A package matches a specific file in which we we can
find at least one compilation unit. The following is a sample O3PRM project:

.. raw:: html

	<embed>
	<table style="width:100%">
		  <tr>
		    <td><code class="docutils literal"><span class="pre">fr</span></code></td>
		    <td>\</td> 
		    <td></td>
		    <td></td>
		    <td></td>
		    <td></td>
		  </tr>
		  <tr>
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td><code class="docutils literal"><span class="pre">lip6</span></code></td>
		    <td>\</td>
		    <td></td>
		    <td></td>
		  </tr>
		  <tr>
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td><code class="docutils literal"><span class="pre">printers</span></code></td>
                    <td>\</td>
		    <td></td>                    
		    <td></td>
		  </tr>
		  <tr>
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td></td>
		    <td>&nbsp&nbsp|</td>
		    <td></td>
		    <td>&nbsp&nbsp|</td>
		    <td><code class="docutils literal"><span class="pre">types.o3prm</span></code></td>
		    <td>// <code class="docutils literal"><span class="pre">types definition </span></code></td>
		  </tr>
		  <tr>
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td></td>
		    <td>&nbsp&nbsp|</td>
		    <td></td>
		    <td>&nbsp&nbsp|</td>
		    <td><code class="docutils literal"><span class="pre">powersupply.o3prm</span></code></td>
		    <td>// <code class="docutils literal"><span class="pre">class PowerSupply definition</span></code></td>
		  </tr>
		  <tr>
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td></td>
		    <td>&nbsp&nbsp|</td>
		    <td><code class="docutils literal"><span class="pre">equipment.o3prm</span></code></td>
		    <td>// <code class="docutils literal"><span class="pre">class Equipment definition</span></code></td>
		  </tr>
		  <tr>
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td></td>
		    <td>&nbsp&nbsp|</td>
		    <td><code class="docutils literal"><span class="pre">room.o3prm</span></code></td>
		    <td>// <code class="docutils literal"><span class="pre">class Room definition</span></code></td>
		  </tr>
		  <tr>
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td></td>
		    <td>&nbsp&nbsp|</td>
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td><code class="docutils literal"><span class="pre">computer.o3prm</span></code></td>
		    <td>// <code class="docutils literal"><span class="pre">class Computer definition</span></code></td>
		  </tr>
		  <tr>
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td></td>
		    <td>&nbsp&nbsp|</td>
		    <td><code class="docutils literal"><span class="pre">printer.o3prm</span></code></td>
		    <td>// <code class="docutils literal"><span class="pre">class Printer definition</span></code></td>
		  </tr>
		  <tr>
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td></td>
		    <td>&nbsp&nbsp|</td>
		    <td><code class="docutils literal"><span class="pre">example.o3prm</span></code></td>
		    <td>// <code class="docutils literal"><span class="pre">system Example definition</span></code></td>
		  </tr>
		  <tr>
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td></td>
		    <td>&nbsp&nbsp|</td> 
		    <td></td>
		    <td>&nbsp&nbsp|</td>
		    <td><code class="docutils literal"><span class="pre">query.o3prm</span></code></td>
		    <td>// <code class="docutils literal"><span class="pre">request query definition</span></code></td>
		  </tr>
	</table>
	</embed>


File extensions must be used as an indicator of the file's compilation unit
nature. The following extensions are allowed: ``.o3prmr`` for queries,
``o3prm`` for everything else (types, classes, interfaces and systems). It is
good practice to name a file with the compilation unit it holds, for example
in ``computer.o3prm`` should contain the definition for the ``Computer``
class. If the file contains several compilation units, you should name the file
according to the unit with the highest order. For example, if you define types,
classes and a system in one file, you should use the system's name.

Compilation units
------------------

There exist four different compilation units in the O3PRM language. A
compilation unit
*declares* a specific element in the modeling process and can either be: an
attribute's type, a class, an interface, a system or even a query. Each
compilation unit can start with a header. Headers are the locations where
you declare ``import`` statements.

.. literalinclude:: code/snippet.o3prm
	:language: bnf
	:lines: 1-6

Header syntax
-------------

Each compilation unit is declared in a module defined by the path from the
project's root to the file's name in which it is declared. Directory separators
are represented using dots. For example the file
``fr/lip6/printers/types.o3prm`` defines the namespace
``fr.lip6.printers.types``. 

Namespaces can be used to import all of the compilation units defined in them,
since many compilation units will need units defined in other files.  In such
cases, we say that a given compilation unit has dependencies which are declared
using the ``import`` keyword. The syntax is:

.. literalinclude:: code/snippet.o3prm
	:language: bnf
	:lines: 7-8

A ``<word>`` is an alphanumerical identifier. You can find its proper
definition in the full BNF.

An example:

.. literalinclude:: code/snippet.o3prm
	:language: o3prm
	:lines: 10

The O3PRM interpreter should use an environment variable to know which
directory to lookup for resolving compilations units. You should check your
O3PRM interpreter to know which environment variable is used.

A compilation unit can be accessed through two different names:

- Its simple name: the name given to it in the file where it is declared (for example ``Computer``). 
- Its full name: defined by its namespace and its simple name (for example ``fr.lip6.printers.Computer``).

In most cases, referring to a compilation unit using its simple name will work,
you will need full names only to prevent name collisions/ambiguities. Name
collisions happen when two compilation units have the same name but are
declared in different namespaces. In such situations, the O3PRM interpreter
cannot resolve the name and will raise an interpretation error.

Note that no matter how you refer to a compilation unit (either by its simple
name or full name) you must always import it using the package complete name.

Finally, note that compilation units are case sensitive regardless of the
operating system.
