================
Type Declaration
================

The O3PRM language offers three kinds of discrete random variables:
categorical (labelized), integer ranged variables and real-valued discretized
variables. Since domains can be shared among attributes in a PRM, the
random variables' domains should be declared in a separate compilation unit
called a ``type``. 

All types declarations start with the keyword ``type`` followed by the type's
name. The variable's domain is enclosed inside parentheses.

Here is the full entry for types in the O3PRM BNF:

.. literalinclude:: code/type.o3prm
	:language: bnf
	:lines: 1-7

Categorical Types
-----------------

Categorical types are used to model categorical random variables, such as
Booleans or colors (``red``, ``green`` and ``blue`` for example). The syntax
is straightforward:

.. literalinclude:: code/type.o3prm
	:language: o3prm
	:lines: 9-10

The boolean type
~~~~~~~~~~~~~~~~

The O3PRM comes with a single built-in type for Boolean random variables. The type is
defined as follows:

.. literalinclude:: code/type.o3prm
	:language: o3prm
	:lines: 12-12

Integer Types
-------------

Integer types are used to model ranges between two integer values. The domain
includes all integers between the lower bound and the upper bound specified.

.. literalinclude:: code/type.o3prm
	:language: o3prm
	:lines: 14-14

Real Types
----------

Real types are used to model discretized continuous variables. There must be
at least three values and each interval is defined as ``]x, y]``. For
example, the following declaration:

.. literalinclude:: code/type.o3prm
	:language: o3prm
	:lines: 16-16

defines the 2-valued discrete random variable defined over ``]0;90]`` and
``]90;180]``. 
