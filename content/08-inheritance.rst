Inheritance
===========

Inheritance is a key aspect of the O3PRM language. O3PRM offers four
different inheritance mechanisms, all with a specific task. Type inheritance
allows to create specialization among random variables' domains. Coupled with
type casting, it can be used to model complex problems. Class and interface
inheritances offer a more traditional inheritance feature. However its
implementation in the O3PRM language adds a lot of expressiveness to
Probabilistic Relational Models. Finally, interface implementation is how we
implemented multiple inheritance.

Type Inheritance
----------------
Subtypes are used to model a ``is a`` relation between types. They are declared
using the ``extends`` keyword. You can only subtype categorical types.

.. literalinclude:: code/inheritance.o3prm
	:language: o3prm
	:lines: 1-2

Here we declared the type ``t_degraded`` as a subtype of ``t_state``. The
mapping notation used inside parentheses indicates how to interpret each
of ``t_degraded`` outcomes as a random variable of type ``t_state``.

Interface Inheritance
---------------------

An interface can extend another one, using the keyword ``extends``. By doing so,
the sub interface inherits all of its super interface attributes and
references.

.. literalinclude:: code/inheritance.o3prm
	:language: o3prm
	:lines: 4-14

Reference Overloading
~~~~~~~~~~~~~~~~~~~~~

When you declare a sub interface, you can overload inherited reference slots.
To do so, the new reference slot type must be a sub class or sub interface of
the reference slot type in the super interface.

.. literalinclude:: code/inheritance.o3prm
	:language: o3prm
	:lines: 16-30

Attribute Overloading
~~~~~~~~~~~~~~~~~~~~~

As for reference overloading, you can overload inherited attributes with a
subtype of the attribute types in the super interface.

.. literalinclude:: code/inheritance.o3prm
	:language: o3prm
	:lines: 32-40
  
Class Inheritance
-----------------

Class inheritance works the same way as inheritance for interfaces with the
additional possibility to overload an inherited attribute's CPT.


Attribute CPT Overloading
~~~~~~~~~~~~~~~~~~~~~~~~~

To overload an inherited attribute's CPT, you simply need to declare an
attribute with a compatible type.

.. literalinclude:: code/inheritance.o3prm
	:language: o3prm
	:lines: 42-48

Multiple Inheritance
--------------------

Classes can implement interfaces using the keyword ``implements``. When a
class implements an interface, it must declare all of the interface's
attributes and reference slots. If the class
implements several interfaces, then it must declare all the attributes and
reference slots of all its interfaces.

.. literalinclude:: code/inheritance.o3prm
	:language: o3prm
	:lines: 50-66

Note that, if a class implements a set of interfaces, then all of its subclasses
also implement the same set of interfaces.

Casting and cast descendants
----------------------------

Casting and cast descendants are how the O3PRM language handles attribute
type overloading and probabilistic dependencies. Attributes types and CPTs
are tightly coupled: the size of a CPT is the product of the domain sizes of its
attribute's type and its parents types. The following example will help us
illustrate why we need casting and casting descendants:

.. literalinclude:: code/inheritance.o3prm
	:language: o3prm
	:lines: 68-98

In this example, we model a water tank overflow problem. We have an interface
describing pumps, a class representing a water tank and an implementation
of interface ``Pump`` for a centrifugal water pump. 

If you look at class ``WaterTank`` you will notice that its attribute
``overflow`` depends on ``Pump`` attribute ``state``, which is of type
``t_state``.

However, in system ``MyPumpSystem``, the reference ``myPump`` of the
instance ``tank`` of Class ``WaterTank`` is assigned to an instance of
class ``CWPump``. Since we overloaded the ``Pump.state`` type by
``t_state`` subtype ``t_degraded``, the CPT definition of attribute
``WaterTank.overflow`` should be incompatible. 

This is not the case here because a cast descendant of attribute ``CWPump.state``
is automatically added to the class ``CWPump``:

.. literalinclude:: code/inheritance.o3prm
	:language: o3prm
	:lines: 100-104

This cast descendant is of the expected type and preserves
``WaterTank.overflow`` CPT's compatibility.

Attributes added automatically are called cast descendants and can be accessed
using the casting notion:

.. literalinclude:: code/inheritance.o3prm
	:language: bnf
	:lines: 106
