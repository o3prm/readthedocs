==================
System Declaration
==================

A system is declared as follows:

.. literalinclude:: code/system_declaration.o3prm
	:language: bnf
	:lines: 1-2

The first ``word`` is the system's name. A system is composed of instance
declarations and assignments. Assignments are used to assign an instance to
an instance's reference slot.  The following illustrates a system declaration:

.. literalinclude:: code/system_declaration.o3prm
	:language: o3prm
	:lines: 4-6

Instance declaration
--------------------

The syntax to declare an instance in a system is:

.. literalinclude:: code/system_declaration.o3prm
	:language: bnf
	:lines: 8

The first ``word`` is the instance's class name and the second is the
instance's name.  For example, if we have a class ``A`` we could declare
the following instance: 

.. literalinclude:: code/system_declaration.o3prm
	:language: o3prm
	:lines: 10

We may want to declare arrays of instances. To do so we need to add ``[n]``
as a suffix to the instance's type, where ``n`` is the number of instances
that the array should contain. if ``n = 0`` then we can simply write ``[]``.

.. literalinclude:: code/system_declaration.o3prm
	:language: o3prm
	:lines: 12-15

You can also specify values for parameters when instantiating a class (see
Section 5.3 on how to parameterize CPTs). The syntax to do so is:

.. literalinclude:: code/system_declaration.o3prm
	:language: bnf
	:lines: 17-19

An example:

.. literalinclude:: code/system_declaration.o3prm
	:language: o3prm
	:lines: 21-22

Assignment
-----------

.. literalinclude:: code/system_declaration.o3prm
	:language: bnf
	:lines: 25-26

It is possible to add instances into an array, using the ``+=`` operator:

.. literalinclude:: code/system_declaration.o3prm
	:language: o3prm
	:lines: 29-38

Reference assignment is done using the ``=`` operator:

.. literalinclude:: code/system_declaration.o3prm
	:language: o3prm
	:lines: 41-55

In the case of multiple references, we can either use the ``=`` to assign
a whole array or the ``+=`` operator to add instances one by one:

.. literalinclude:: code/system_declaration.o3prm
	:language: o3prm
	:lines: 58-86
