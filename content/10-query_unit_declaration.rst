.. |_| unicode:: 0xA0
	:trim:

======================
Query unit declaration
======================

A query unit is defined using the keyword ``request``. Its syntax is the
following:

.. literalinclude:: code/query_unit_declaration.o3prmr
	:language: bnf
	:lines: 1-6

The first ``word`` is the query's name.  In a query unit we can alternate
between observations and queries. An observation, also called an *evidence*,
allows to specify the value that we observe for a given random variable
(e.g., we observe on our thermometer that variable ``temperature`` is equal
to 20 degrees Celsius). Evidence are assigned to their corresponding random
variables using the ``=`` operator. A query over random variable ``X`` asks
to infer the probability *P(X|\e)* where *\e* represents the set of all the
evidence specified so far in the request unit. This is done using the ``?``
operator. The ``unobserve`` keyword can be used to remove evidence inside
the request unit.

.. literalinclude:: code/query_unit_declaration.o3prmr
	:language: o3prm
	:lines: 8-18

For instance, in the above example, the first query over random variable
``mySystem.anObject.anotherVariable`` returns the *posterior* of the
variable given the three evidence entered into the system. The second
query returns the *posterior* of the same variable given only the last two
evidence entered, the first one being invalidated by the ``unobserve``
instruction. 
