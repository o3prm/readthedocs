========
Examples
========

The Water Sprinkler
-------------------

.. literalinclude:: code/water_sprinkler.o3prm
	:language: o3prm

The Printer Example
-------------------

.. literalinclude:: code/printers.o3prm
	:language: o3prm

Printers with inheritance
-------------------------

.. literalinclude:: code/complex_printers.o3prm
	:language: o3prm
