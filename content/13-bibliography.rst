Bibliography
------------

J. Pearl. Probabilistic Reasoning in Intelligent Systems: Networks of Plausible Infer
ence. Morgan Kaufman, 1988.

Daphne Koller and Avi Pfeffer. Object-oriented bayesian networks. In Proceedings of
the 13th Annual Conference on Uncertainty in AI, pages 302–313, 1997.

Daphne Koller and Avi Pfeffer. Probabilistic frame-based systems. In Proceedings of the
Fifteenth National Conference on Artificial Intelligence (AAAI-98), pages 580–587,
1998.

Avi Pfeffer. Probabilistic Reasoning for Complex Systems. PhD thesis, Stanford Uni
versity, 1999.

Olav Bangsø and Pierre-Henri Wuillemin. Object oriented bayesian networks: A frame
work for topdown specification of large bayesian networks and repetitive structures.
Technical report, Department of Computer Science, Aalborg University, 2000.

Olav Bangsø and Pierre-Henri Wuillemin. Top-down construction and repetitive struc
tures representation in bayesian networks. In Proceedings of the 13th Florida Artifi
cial Intelligence Research Society Conference, 2000.

Olav Bangsø. Object Oriented Bayesian Networks. PhD thesis, Aalborg University,
March 2004.

Lise Getoor, Nir Friedman, Daphne Koller, Avi Pfeffer, and Ben Taskar. Probabilistic
relational models. In L. Getoor and B. Taskar, editors, An Introduction to Statistical
Relational Learning. MIT Press, 2007.

D. Koller and N. Friedman. Probabilistic Graphical Models: Principles and Techniques.
MIT Press, 2009.

Judea Pearl. Causality. Cambridge University Press, 2009.

Lionel Torti, Pierre-Henri Wuillemin, and Christophe Gonzales. Reinforcing the object
oriented aspect of probabilistic relational models. In Teemu Roos Petri Myllymäki
and Tommi Jaakkola, editors, Proceedings of the The Fifth European Workshop on
Probabilistic Graphical Models. HIIT Publications, 2010.

Lionel Torti. Structured probabilistic inference in object-oriented
probabilistic graphical models. PhD Thesis, Université Pierre et Marie Curie, 2012.