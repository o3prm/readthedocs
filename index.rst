.. o3prm documentation master file, created by
   sphinx-quickstart on Thu Oct 19 14:06:24 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

O3PRM's documentation
=================================

The O3PRM language's purpose is to model Probabilistic Relational Models (PRMs)
using a strong object oriented syntax.

.. toctree::
   :caption: Table of contents
   :numbered:

   content/01-intro
   content/02-tutorial
   content/03-structure
   content/04-type_declaration
   content/05-class_declaration
   content/06-interface_declaration
   content/07-functions
   content/08-inheritance.rst
   content/09-system_declaration
   content/10-query_unit_declaration
   content/11-o3prm_bnf
   content/12-examples
   content/13-bibliography
